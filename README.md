Jcode Setup
===========

Opinionated personal [Lubuntu](https://lubuntu.me/) 22.04 (LTS) coding environment setup for several different programming languages and other software development tools implemented as an [Ansible](https://www.ansible.com/) playbook. The playbook is not 100% foolproof but works correctly enough.

After setting up Lubuntu install Ansible:

```
# 22.04 bug: https://bugs.launchpad.net/ubuntu/+source/ansible-core/+bug/1969917
# sudo apt install -y ansible-core

# workaround
sudo apt install -y ansible
```

Add your public key to Github and authenticate (some Ansible roles will download stuff from Github):

```
ssh -T git@github.com
```

How to run:

```
ansible-playbook -vv jcode.yml --connection=local --ask-become-pass
```

By default only the following roles are run:

* common
* editors (includes GNU [Emacs](https://www.gnu.org/software/emacs/), Microsoft [Visual Studio Code](https://code.visualstudio.com/), JetBrains [IntelliJ IDEA](https://www.jetbrains.com/idea/) Community Edition)
* personal configuration

All other roles are skipped unless they are specifically requested with `--tags`:

* ballerina
* bbcbasic
* docker
* elixir
* kotlin
* oracle-tools (for database development)
* postgresql
* specnext (not yet updated to 22.04)
* v (deprecated, to be removed in 22.04)
* web
* xml
* z80 (deprecated, use specnext instead, to be removed in 22.04)

Note About Oracle Tools
-----------------------

SQLcl can be downloaded automatically from Oracle site but SQL Developer can't. Thus one have to [download](https://www.oracle.com/database/sqldeveloper/technologies/download/) the SQL Developer installation package (select "Other Platforms") manually and save it to [`roles/oracle-tools/files`](roles/oracle-tools/files) directory. Remember to set the `sqldeveloper_version` variable in [`roles/oracle-tools/defaults/main.yml`](roles/oracle-tools/defaults/main.yml) to match the downloaded package.

Terminals
---------

The preferred terminal emulator is [kitty](https://sw.kovidgoyal.net/kitty/).

Linux consoles:

* Graphical console: `Ctrl-Alt-F1`
* Text console: `Ctrl-Alt-F2`
