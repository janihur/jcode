# add kotlin compiler to path
kotlin_bin_path="/opt/kotlinc/bin"
if [[ -n "${PATH##*${kotlin_bin_path}}" && -n "${PATH##*${kotlin_bin_path}:*}" ]]
then
    export PATH="$PATH:${kotlin_bin_path}"
fi
