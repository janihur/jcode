#!/usr/bin/bash

pushd /opt/zesarux-9.2 > /dev/null

if [[ "$1" == 'next' ]]
then # ZX Spectrum Next
    ./zesarux --machine tbblue --enable-mmc --enable-divmmc-ports --mmc-file tbblue.mmc --realvideo
else # ZX Spectrum 48k
    ./zesarux --machine 48k
fi

popd > /dev/null
