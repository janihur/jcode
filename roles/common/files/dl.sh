#!/bin/bash

if [[ $# -eq 2 ]]
then
    file=$1
    url=$2
    curl -L -o $file $url
elif [[ $# -eq 1 ]]
then
    url=$1
    curl -L $url
fi
